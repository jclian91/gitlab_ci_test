This is a Python project for Gitlab CI/CD.

### Quickstart

1. install packages in `requirements.txt`
2. run `python server.py`
3. the http url is http://localhost:5000

### Docker

1. build docker image: 

```commandline
docker build -t flask_blue_print .
```

2. run docker image:

```commandline
docker run -d -p 5005:5000 flask_blue_print
```