# -*- coding: utf-8 -*-
# @place: Pudong, Shanghai
# @file: func_add.py
# @time: 2023/8/19 15:37
def add(a, b):
    if isinstance(a, str) and isinstance(b, str):
        return a + '+' + b
    elif isinstance(a, list) and isinstance(b, list):
        return a + b
    elif isinstance(a, (int, float)) and isinstance(b, (int, float)):
        return a + b
    else:
        return None