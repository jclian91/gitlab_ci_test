# -*- coding: utf-8 -*-
# @place: Pudong, Shanghai
# @file: test_func_add.py
# @time: 2023/8/19 15:38
import unittest

from func_add import add


class TestAdd(unittest.TestCase):
    def setUp(self):
        pass

    def test_add_case1(self):
        a = "Hello"
        b = "World"
        res = add(a, b)
        print(res)
        self.assertEqual(res, "Hello+World")

    def test_add_case2(self):
        a = 1
        b = 2
        res = add(a, b)
        print(res)
        self.assertEqual(res, 3)

    def test_add_case3(self):
        a = [1, 2]
        b = [3]
        res = add(a, b)
        print(res)
        self.assertEqual(res, [1, 2, 3])

    def test_add_case4(self):
        a = 2
        b = "3"
        res = add(a, b)
        print(None)
        self.assertEqual(res, None)


if __name__ == '__main__':

    # 部分用例测试
    # 构造一个容器用来存放我们的测试用例
    suite = unittest.TestSuite()
    # 添加类中的测试用例
    suite.addTest(TestAdd('test_add_case1'))
    suite.addTest(TestAdd('test_add_case2'))
    suite.addTest(TestAdd('test_add_case3'))
    suite.addTest(TestAdd('test_add_case4'))
    run = unittest.TextTestRunner()
    run.run(suite)
