FROM python:3.9-alpine3.17

LABEL author="jclian91"

RUN apk add --no-cache gcc musl-dev openssl-dev libffi-dev
COPY . /root/src
WORKDIR /root/src
ENV my=test
RUN pip install -r requirements.txt
EXPOSE 5000
ENTRYPOINT ["sh", "scripts/dev.sh"]