# -*- coding: utf-8 -*-
import time

from flask import Flask

app = Flask(__name__)


@app.route('/')
def index():
    return "Hello index"


@app.route('/test')
def test():
    return "Hello test"


@app.route('/random_sleep')
def random():
    time.sleep(1)
    return "random"


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=5000, debug=False)
